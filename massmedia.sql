-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema massmedia
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema massmedia
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `massmedia` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `massmedia` ;

-- -----------------------------------------------------
-- Table `massmedia`.`product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `massmedia`.`product` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL,
  `price` FLOAT(10,2) NULL,
  `amount` MEDIUMINT(5) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `massmedia`.`box`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `massmedia`.`box` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `massmedia`.`product_in_box`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `massmedia`.`product_in_box` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `box_id` INT NULL,
  `product_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `box_id` (`box_id` ASC),
  INDEX `product_id` (`product_id` ASC),
  CONSTRAINT `box_id`
    FOREIGN KEY (`box_id`)
    REFERENCES `massmedia`.`box` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `product_id`
    FOREIGN KEY (`product_id`)
    REFERENCES `massmedia`.`product` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `massmedia`.`product`
-- -----------------------------------------------------
START TRANSACTION;
USE `massmedia`;
INSERT INTO `massmedia`.`product` (`id`, `name`, `price`, `amount`) VALUES (1, 'Toode 1', 4.30, 3);
INSERT INTO `massmedia`.`product` (`id`, `name`, `price`, `amount`) VALUES (2, 'Toode 2', 6.12, 1);
INSERT INTO `massmedia`.`product` (`id`, `name`, `price`, `amount`) VALUES (3, 'Toode 3', 1.11, 0);
INSERT INTO `massmedia`.`product` (`id`, `name`, `price`, `amount`) VALUES (4, 'Toode 4', 5.00, 4);
INSERT INTO `massmedia`.`product` (`id`, `name`, `price`, `amount`) VALUES (5, 'Toode 5', 2.25, 1);
INSERT INTO `massmedia`.`product` (`id`, `name`, `price`, `amount`) VALUES (6, 'Toode 6', 7.22, 3);
INSERT INTO `massmedia`.`product` (`id`, `name`, `price`, `amount`) VALUES (7, 'Toode 7', 67, 9);
INSERT INTO `massmedia`.`product` (`id`, `name`, `price`, `amount`) VALUES (8, 'Toode 8', 15.83, 11);
INSERT INTO `massmedia`.`product` (`id`, `name`, `price`, `amount`) VALUES (9, 'Toode 9', 4.24, 3);
INSERT INTO `massmedia`.`product` (`id`, `name`, `price`, `amount`) VALUES (10, 'Toode 10', 0.45, 2);

COMMIT;


-- -----------------------------------------------------
-- Data for table `massmedia`.`box`
-- -----------------------------------------------------
START TRANSACTION;
USE `massmedia`;
INSERT INTO `massmedia`.`box` (`id`, `name`) VALUES (1, 'Kast 1');
INSERT INTO `massmedia`.`box` (`id`, `name`) VALUES (2, 'Kast 2');
INSERT INTO `massmedia`.`box` (`id`, `name`) VALUES (3, 'Kast 3');
INSERT INTO `massmedia`.`box` (`id`, `name`) VALUES (4, 'Kast 4');
INSERT INTO `massmedia`.`box` (`id`, `name`) VALUES (5, 'Kast 5');
INSERT INTO `massmedia`.`box` (`id`, `name`) VALUES (6, 'Kast 6');
INSERT INTO `massmedia`.`box` (`id`, `name`) VALUES (7, 'Kast 7');

COMMIT;


-- -----------------------------------------------------
-- Data for table `massmedia`.`product_in_box`
-- -----------------------------------------------------
START TRANSACTION;
USE `massmedia`;
INSERT INTO `massmedia`.`product_in_box` (`id`, `box_id`, `product_id`) VALUES (1, 3, 2);
INSERT INTO `massmedia`.`product_in_box` (`id`, `box_id`, `product_id`) VALUES (2, 3, 4);
INSERT INTO `massmedia`.`product_in_box` (`id`, `box_id`, `product_id`) VALUES (3, 3, 6);
INSERT INTO `massmedia`.`product_in_box` (`id`, `box_id`, `product_id`) VALUES (4, 6, 2);
INSERT INTO `massmedia`.`product_in_box` (`id`, `box_id`, `product_id`) VALUES (5, 6, 4);
INSERT INTO `massmedia`.`product_in_box` (`id`, `box_id`, `product_id`) VALUES (6, 1, 1);
INSERT INTO `massmedia`.`product_in_box` (`id`, `box_id`, `product_id`) VALUES (7, 1, 10);
INSERT INTO `massmedia`.`product_in_box` (`id`, `box_id`, `product_id`) VALUES (8, 2, 2);
INSERT INTO `massmedia`.`product_in_box` (`id`, `box_id`, `product_id`) VALUES (9, 4, 9);
INSERT INTO `massmedia`.`product_in_box` (`id`, `box_id`, `product_id`) VALUES (10, 4, 7);
INSERT INTO `massmedia`.`product_in_box` (`id`, `box_id`, `product_id`) VALUES (11, 4, 4);
INSERT INTO `massmedia`.`product_in_box` (`id`, `box_id`, `product_id`) VALUES (12, 5, 1);

COMMIT;

