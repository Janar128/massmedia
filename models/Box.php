<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "box".
 *
 * @property integer $id
 * @property string $name
 *
 * @property ProductInBox[] $productInBoxes
 */
class Box extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'box';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])
			->viaTable('product_in_box', ['box_id' => 'id']);
    }
}
