<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_in_box".
 *
 * @property integer $id
 * @property integer $box_id
 * @property integer $product_id
 *
 * @property Box $box
 * @property Product $product
 */
class ProductInBox extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_in_box';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['box_id', 'product_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'box.name' => 'Box name',
            'product.name' => 'Product Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBox()
    {
        return $this->hasOne(Box::className(), ['id' => 'box_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
