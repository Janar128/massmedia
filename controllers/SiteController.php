<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Product;

class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
    	//Get products for main table list
    	$model = Product::find()->all();
		
        return $this->render('index', [
            'model' => $model,
        ]);
    }
	
	public function actionAmount() {
		//Change retur to json
		Yii::$app->response->format = 'json';
		//Get post data
		$data = Yii::$app->request->post();
		
		//check if id exists
		if(!empty($data['id'])) {
			//Find selected product
			$model = $this->findModel($data['id']);
			
			//Change product amount
			if($model->load(array('Product' => array('amount' => $data['amount']))) && $model->save()) {
				return array('message' => 'Amount changed', 'status' => 'OK');
			}
		}
		
		//Return error if request failed
		return array('message' => 'Failed to update amount!', 'status' => 'Error');
	}
	
	//Fign Product model with id
	protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
