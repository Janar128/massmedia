<?php

namespace app\controllers;

use Yii;
use app\models\ProductInBox;
use app\models\Product;
use app\models\Box;
use app\models\ProductInBoxSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * ProductInBoxController implements the CRUD actions for ProductInBox model.
 */
class ProductInBoxController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProductInBox models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductInBoxSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProductInBox model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProductInBox model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductInBox();
		
		//Create product array for better product selection
		$products = Product::find()->asArray()->all();
		$products = ArrayHelper::map($products, 'id', 'name');
		
		//Create product array for box product selection
		$boxes = Box::find()->asArray()->all();
		$boxes = ArrayHelper::map($boxes, 'id', 'name');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'products' => $products,
                'boxes' => $boxes
            ]);
        }
    }

    /**
     * Updates an existing ProductInBox model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		
		//Create product array for better product selection
		$products = Product::find()->asArray()->all();
		$products = ArrayHelper::map($products, 'id', 'name');
		
		//Create product array for better product selection
		$boxes = Box::find()->asArray()->all();
		$boxes = ArrayHelper::map($boxes, 'id', 'name');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'products' => $products,
                'boxes' => $boxes
            ]);
        }
    }

    /**
     * Deletes an existing ProductInBox model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProductInBox model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductInBox the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductInBox::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
