<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <table class="table table-stiped table-hover productTable">
    	<thead>
    		<tr>
    			<th>Name</th>
    			<th>Boxes</th>
    			<th>Price</th>
    			<th>Amount</th>
    			<th>Total</th>
    		</tr>
    	</thead>
    	<tbody>
    		<?php
			foreach ($model as $product) {
				$data = $product->getAttributes();
				$boxes = $product->getBoxes()->all();
				?>
				<tr class="productRow">
					<td><?php echo $data['name']; ?></td>
					<td><?php
						foreach ($boxes as $key => $box) {
							$box = $box->getAttributes();
							if($key != 0) echo ', ';
							echo $box['name'];
						}
					?></td>
					<td class="priceCol"><?php echo $data['price']; ?> €</td>
					<td class="input-group input-group-xs quantityHolder">
						<span class="input-group-btn">
							<button class="btn btn-danger btn-xs quantityBtn minus" <?= $data['amount'] == 0 ? 'disabled="disabled"' : ''; ?>">
								<span class="glyphicon glyphicon-minus"></span>
							</button>
						</span>
						<input type="text" class="form-control quantity" value="<?php echo $data['amount']; ?>" data-url="<?php echo Yii::$app->getUrlManager()->createUrl(['site/amount/']) ?>" data-id="<?php echo $data['id']; ?>">
						<span class="input-group-btn">
							<button class="btn btn-success btn-xs quantityBtn plus">
								<span class="glyphicon glyphicon-plus"></span>
							</button>
						</span>
						<div class="status"></div>
					</td>
					<td class="totalCol"><?php echo $data['amount'] * $data['price']; ?> €</td>
				</tr>
				<?php
			}
    		?>
    	</tbody>
    </table>

</div>			
