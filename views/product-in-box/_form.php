<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductInBox */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-in-box-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'box_id')->dropDownList($products) ?>

    <?= $form->field($model, 'product_id')->dropDownList($boxes)  ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
