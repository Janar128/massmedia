<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductInBox */

$this->title = 'Update Product In Box: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Product In Boxes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="product-in-box-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'products' => $products,
        'boxes' => $boxes
    ]) ?>

</div>
