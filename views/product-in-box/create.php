<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProductInBox */

$this->title = 'Create Product In Box';
$this->params['breadcrumbs'][] = ['label' => 'Product In Boxes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-in-box-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'products' => $products,
        'boxes' => $boxes
    ]) ?>

</div>
