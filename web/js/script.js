//Chech if value is int else return 0
function toInt(val) {
    return ($.isNumeric(val) && Math.floor(val) == val) ? val : 0;
}

//Replace € and change value to float
function colToVal(val) {
    return parseFloat(val.replace(' €', ''));
}

//Send ajax request after 1s wait
var amountTimer;
function updateAmount(input) {
    clearTimeout(amountTimer);
    if(!input.hasClass('error')) {
        amountTimer = setTimeout(function() {
            var data = {
                id: input.attr('data-id'),
                amount:input.val()
            };
            $.ajax ({
                type: 'POST',
                url: input.attr('data-url'),
                dataType: 'json',
                data: data,
                success: function(response) {
                    if(response.status == 'OK') {
                        //calculate rows total with new amount
                        var tr = input.closest('.productRow');
                        var price = colToVal(tr.find('.priceCol').html());
                        
                        var total = price * parseInt(input.val());
                        
                        tr.find('.totalCol').html(total.toFixed(2) + ' €');
                    }
                    else
                        alert(response.message);
                     
                },
                error: function(xhr, textStatus, err) {
                    alert('Request failed');
                }
            });
        }, 1000);
    }
}

//amount change button click
$('.quantityBtn').on('click', function(e) {
   var wrap = $(this).closest('.quantityHolder');
   var input = wrap.find('.quantity');
   var value = parseInt(toInt(input.val()));
    
   if($(this).hasClass('plus')) {
       //add 1 to amount
       input.val(value+1);
       
       //If amount previously was 0 activate minus button
       if(value == 0)
           wrap.find('.quantityBtn.minus').prop('disabled',false);
   }
   else if(value != 0) {
       //remove 1 from amount
       input.val(value-1);
       
       //If previously amount will by 0 disable minus button
       if(value == 1) 
           wrap.find('.quantityBtn.minus').prop('disabled',true);
   }
   
   //call amount update request
   updateAmount(input);
});

$(document).on('keyup', '.quantity', function(e) {
    var wrap = $(this).closest('.quantityHolder');
    var input = wrap.find('.quantity');
    var value = parseInt(toInt(input.val()));
    
    //check value content and add error class if contains somthing else than numbers
    if(value != input.val() && !input.hasClass('error')) {
        input.addClass('error');
    }
    //Remove error class if content only numbers
    else if(value == input.val() && input.hasClass('error')) {
        input.removeClass('error');
    }
    
    if(!input.hasClass('error')) {
        //disable minus button when amount 0
        if(value == 0)
            wrap.find('.quantityBtn.minus').prop('disabled',true);
        else
            wrap.find('.quantityBtn.minus').prop('disabled',false);
            
        //call amount update request
        updateAmount(input);
    }
});
